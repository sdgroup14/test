import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.scss']
})
export class Test2Component implements OnInit {
  // items = [];
  items = Array.from({length: 5000000}).map((_, i) => `Item #${i}`);

  // trackByFn(index, item) {
  //   return index; // or item.id
  // }

  constructor() {
    // for (let i = 0; i < 100000; i++) {
    //   this.items.push({name: 'Jhon', years: 20});
    // }
  }

  ngOnInit(): void {
  }

}
